---
title: "Documentation"
linkTitle: "Documentation"
weight: 20 
menu:
   main:
      weight: 20
---

{{% pageinfo %}} This is still a work in progress documentation {{% /pageinfo %}}

Welcome to the Jedidex API, the complete StarWars API! This documentation should help you familiarise with the resources
available and how to consume them with HTTP requests.