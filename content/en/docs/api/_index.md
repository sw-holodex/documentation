---
title: "API GUI"
linkTitle: "API GUI"
type: swagger 
weight: 2 
menu:
    main:
        weight: 40
---
<style>
.td-content pre{
  background-color: #0000;
}

</style>

<link rel="stylesheet" href="/outline-swagger.css" />

{{< swaggerui src="https://pyapi.jedidex.com/swagger.json" >}}
